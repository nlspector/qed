package surface;

import core.Main;
import core.Point;

public class Mirror implements Surface{

	double m,xmin,xmax;
	Point e1,e2;
	
	public Mirror(double x1, double x2) {
		this(new Point(x1,0), new Point(x2,0));
	}
	
	public Mirror(Point e1, Point e2) {
		m=(e2.y-e1.y)/(e2.x-e1.x);
		if(e1.x < e2.x) {
			xmin=e1.x;
			xmax=e2.x;
		} else {
			xmin=e2.x;
			xmax=e1.x;
		}
		this.e1=e1;
		this.e2=e2;
	}
	
	@Override
	public Point getIntersection(Point origin, Point ray) {
		double mray=ray.y/ray.x;
		if(mray == m) return null;
		double x = (mray*origin.x-m*e1.x+e1.y-origin.y)/(mray-m);
		if(x < xmin || x > xmax) return null;
		return new Point((float) x, (float)(m*x-m*e1.x+e1.y));
		
	}
	
	public boolean offMirror(Point p) {
		return (p.x < xmin || p.x > xmax);
	}
	
	public void render(Main m) {
		m.stroke(255);

		m.strokeWeight(0.02f);
		m.line((float) e1.x, (float) e1.y, (float) e2.x, (float) e2.y);
	}
	

}
