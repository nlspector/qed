package surface;

import core.Point;

public interface Surface {

	public Point getIntersection(Point origin, Point ray);
	
}
