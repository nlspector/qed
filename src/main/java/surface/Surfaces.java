package surface;

import java.util.ArrayList;
import java.util.List;

public class Surfaces {
	
	static List<Surface> surfaces=new ArrayList<>();
	
	private Surfaces() {
		
	}
	
	public static List<Surface> getSurfaces(){
		return surfaces;
	}
	
	public static void addSurface(Surface s) {
		surfaces.add(s);
	}

}
