package surface;

import java.util.ArrayList;
import java.util.List;

import core.Main;
import core.PathCalculator;
import core.Point;

public class DiffractionMirror extends Mirror{

	double resolution;
	
	List<Interval> intervals = new ArrayList<>();
	
	boolean left=false;
	
	public DiffractionMirror(double x1, double x2, double res, PathCalculator pc) {
		super(x1, x2);
		resolution=res;
		
		double start = 0;
		
		for(double d = xmin; d < xmax; d+=resolution) {
			if(pc.wavePath(d) < pc.states/2) {
				if(!left) {
					left=true;
					start=d;
				} 
			} else {
				if(left) {
					left=false;
					intervals.add(new Interval(start,d));
				}
			}
		}
		if(left) {
			intervals.add(new Interval(start,xmax));

		}
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Point getIntersection(Point origin, Point ray) {
		Point p = super.getIntersection(origin,ray);
		if(p==null)return null;
		for(Interval i : intervals) {
			if(i.within(p.x)) return p;
		}
		return null;
	}
	
	
	@Override
	public void render(Main m) {
		m.stroke(255);
		m.strokeWeight(0.015f);

		for(Interval i : intervals) {
			m.line((float) i.d1, (float) 0, (float) i.d2, (float) 0);

		}

	}
	
//	@Override
//	public boolean offMirror(Point p) {
//		for(Interval i : intervals) {
//			if(i.within(p.x)) return false;
//		}	
//		return true;
//	}

	
	
	public class Interval {
		double d1,d2;
		public Interval(double d1, double d2) {
			this.d1=Math.min(d1,d2);
			this.d2=Math.max(d1,d2);
		}
		
		public boolean within(double d3) {
			return (d3 > d1 && d3 < d2);
		}
		
	}
	
	
	
}
