package core;

public class PathCalculator implements Runnable{

	int runs;
	
	double[] vals;
	Point start,end;
	
	public static int states=3600;
	
	double wavelength;
	
	public PathCalculator(Point start, Point end, int runs) {
		this.runs=runs;
		vals = new double[runs];
		

		this.start=start;
		this.end=end;
	}
	
	public PathCalculator withWavelength(double wl) {
		wavelength=wl;
		return this;
	}
	
	@Override
	public void run() {
		for(int i = 0; i < runs; i++) {
			vals[i] = new Path(start, end, (3.14*i)/((float)runs)).run();
		}
	}
	
	public int runPoint(Point pt) {
		return 0;
	}
	
	public int wave(double val) {
		return (int)((val % wavelength)/wavelength*states);
	}
	
	public int wavePath(double pt) {
		return wave(new Path(start,end,new Point(pt,0)).run());
	}
	
	public double[] wavelength() {
		double[] waves = new double[states];

		for(int i = 0; i < runs; i++) {
			if(vals[i] != -1) {
				//if(i%10000==0)System.out.println(i);

				int ind = wave(vals[i]);
				//if(Math.abs(vals[i]-2.828) < 0.001) System.out.println(ind);
				//if(ind == 22) System.out.println(vals[i] + " " + (vals[i] % wavelength));
				waves[ind]=waves[ind]+1;
			}
		}
//		for(int i = 0; i < states; i++) {
//			System.out.println(i + " " + waves[i]);
//		}
		return waves;
	}

}
