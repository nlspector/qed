package core;

import processing.core.PApplet;
import processing.core.PVector;
import surface.Mirror;

public class GraphWindow extends PApplet{
	public GraphWindow() {
		PApplet.runSketch(new String[]{this.getClass().getSimpleName()}, this);
		//xLabel = x;
		//yLabel = y;
	}
	@Override
	public void settings() {
		size(960,640);
	}
	
	int ind=0;
	
	@Override
	public void draw() {
		

		background(0);
		fill(255);
		strokeWeight(0);
		stroke(0);
		for(int i = 0; i < Main.waves.length; i++) {
			ellipse(i*width/(float)(Main.waves.length),height-(float) (Main.waves[i])*5,2,2);
		}
		strokeWeight(2);
		stroke(255,0,0);
		line(ind*width/(float)(Main.waves.length),0,ind*width/(float)(Main.waves.length),height);
		
		drawArrow(width/2,height/2,ind/((float)PathCalculator.states)*Math.PI*2,50);
		
		PVector res = Main.calcResultant();
		//System.out.println(res.mag());
		drawArrow(width/2+50,height/2+50,PVector.angleBetween(new PVector(1,0), res),res.mag()/100f);

	}
	
	
	public void drawArrow(int x, int y, double ang, float length) {
		pushMatrix();
		translate(x,y);
		fill(255,255,0);
		stroke(255,255,0);
		rotate((float)-ang);
		line(0,0,length,0);
		triangle(length,0,length-length/5f,length/10f, length-length/5f, -length/10f);

		popMatrix();
	}
	
	public void setInd(int ind) {
		this.ind=ind;
	}
	
}
