package core;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import surface.DiffractionMirror;
import surface.Mirror;

public class Main extends PApplet{
	
	/** QED Simulation
	 * 
	 * Assumptions made:
	 * 1) Straight-line paths only
	 * 2) only 3600 possible "clock" positions
	 * 3) only 200k runs, even distributed from 0 to pi degrees
	 *    i.e., ignore paths that don't intersect the mirror
	 */
	public static double extent=2;
	public static Mirror mirror = new Mirror(-extent,extent);
	static Point start = new Point(0,1);
	static Point end = new Point(2,1);
	
	public float scale = 200;
	
	public static int runs=200000;
	
	public static double[] vals = new double[runs];
	public static int ind=0;
	
	public static double[] waves = new double[1];
	GraphWindow gw;
	PathCalculator pc;
	
	Point demoPt=new Point(0,0);

	static PVector[] vecs=new PVector[1800];
	
	boolean diff=false;
	
	PImage img;
	
	public static void main(String[] args) {
		PApplet.main(new String[] {Main.class.getName()});
//		//139 ms for 1m
//		long time = System.currentTimeMillis();
//		for(int i = 0; i < 1000000; i++) {
//			new Path(new Point(-1,1), new Point(1,1)).run();
//			if(i%100000==0)	System.out.println(i+ " " +(System.currentTimeMillis()-time));
//
//		}
//		System.out.println(System.currentTimeMillis()-time);
		
		//calcPaths();
	}
	
	@Override
	public void settings() {
		size(960,640);
		gw = new GraphWindow();
		
		
	}
	
	public void setup() {
		img = loadImage("aah.png");
		for(int i = 0; i < 1800; i++) {
			vecs[i]=PVector.fromAngle(i/1800f*3.14159f);
		}
	}
	
	public void calcPaths() {
		//long time = System.currentTimeMillis();
		
		//System.out.println(System.currentTimeMillis()-time);
		//time = System.currentTimeMillis();
		pc = new PathCalculator(start, end,runs).withWavelength(7E-3);
		pc.run();
		waves=pc.wavelength();
		//System.out.println(System.currentTimeMillis()-time);
	}
	
	@Override
	public void draw() {
		calcPaths();
		if(!diff) mirror = new Mirror(-extent,extent);

		else mirror = new DiffractionMirror(-extent,extent,0.0001,pc);
		
		
		//rendering mirror etc
		background(0);
		textSize(24);
		fill(255);
		
		//text at top
		text("extent: " + ((int)(extent*100))/100d, 0, 24);
		
		text("demo point: " + ((int)(demoPt.x*1000))/1000d, 0, 48);

		
		translate(width/2, height/2);
		scale(scale,-scale);
		mirror.render(this);
		
		//render start and end points
		fill(255,200,200);
		stroke(0);
		strokeWeight(0.05f);
		ellipse((float)start.x,(float)start.y,0.1f,0.1f);
		
		fill(200,255,200);
		ellipse((float)end.x,(float)end.y,0.1f,0.1f);
		
		
		//render demopath
		stroke(255);
		strokeWeight(0.005f);
		if(!mirror.offMirror(demoPt)) {
			line((float)start.x,(float)start.y,(float)demoPt.x,0);
			line((float)end.x,(float)end.y,(float)demoPt.x,0);
	
			//tell graphwindow to draw red line
			gw.setInd(pc.wavePath(demoPt.x));
		} else {
			double offscreen = -2;
			double m = start.y/(start.x-demoPt.x);
			line((float)start.x,(float)start.y,(float)((offscreen-start.y)/m + start.x),(float)offscreen);
			
			gw.setInd(-100);
		}
		
		
	}
	
	public static PVector calcResultant() {
		PVector res = new PVector(1,0,0);
		if(waves.length ==3600)
		for(int i = 0; i < 1800; i++) {
			res.add(PVector.mult(vecs[i], (float) (waves[i]-waves[1800+i])));
		}
		return res;
	}
	
	@Override
	public void keyTyped() {
		if(key == 'w') {
			extent+=0.01;
		} else if(key == 's') {
			extent-=0.01;
		} else if(key == 'e') {
			extent+=0.1;
		} else if(key == 'd') {
			extent-=0.1;
		} 
		//controlling demoPt.x
		else if(key == 'v') {
			demoPt.x-=0.1;
		} else if(key == 'm') {
			demoPt.x+=0.1;
		} else if(key == 'b') {
			demoPt.x-=0.01;
		} else if(key == 'n') {
			demoPt.x+=0.01;
		}else if(key == 'j') {
			demoPt.x-=0.001;
		} else if(key == 'k') {
			demoPt.x+=0.001;
		} 
		
		//diffraction or not
		else if(key == ' ') {
			diff=!diff;
		}
	
	}
	
	

}
