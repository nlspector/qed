package core;


public class Path {
	
	Point start,end,inter;
	double ang;
	
	
	public Path(Point start, Point end) {
		this.start=start;
		this.end=end;
		ang = Math.random()*Math.PI;
	}
	
	public Path(Point start, Point end, double ang) {
		this.start=start;
		this.end=end;
		this.ang = ang;
	}
	
	public Path(Point start, Point end, Point inter) {
		this.start=start;
		this.end=end;
		this.inter=inter;
	}

	public double run() {
		
		if(inter==null) {
			Point ray = new Point(Math.cos(ang), Math.sin(ang));
			inter = Main.mirror.getIntersection(start,ray);
		}
		if(inter != null) {
			return Point.dist(start,inter) + Point.dist(inter,end);
			
		} else return -1;
	}

}
